const funcs = {
    degreesToRads(deg) {
        return (deg * Math.PI) / 180.0
    },

    radsToDegrees(rad) {
        return (rad * 180.0) / Math.PI
    },
}

module.exports = funcs