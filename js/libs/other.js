const funcs = {
    // Check type của dữ liệu biến
    // Exp: getType(new Set([1, 2, 3])); // 'set'
    getType(v) {
        return v === undefined ? 'undefined' : v === null ? 'null' : v.constructor.name.toLowerCase()
    },

    // Check type
    // is(Array, [1]); // true
    // is(ArrayBuffer, new ArrayBuffer()); // true
    // is(Map, new Map()); // true
    // is(RegExp, /./g); // true
    // is(Set, new Set()); // true
    // is(WeakMap, new WeakMap()); // true
    // is(WeakSet, new WeakSet()); // true
    // is(String, ''); // true
    // is(String, new String('')); // true
    // is(Number, 1); // true
    // is(Number, new Number(1)); // true
    // is(Boolean, true); // true
    // is(Boolean, new Boolean(true)); // true
    is(type, val) {
        return ![, null].includes(val) && val.constructor === type
    },

    // Check dateA là ngày mai của dateB
    // Exp: isAfterDate(new Date(2010, 10, 21), new Date(2010, 10, 20)); // true
    isAfterDate(dateA, dateB) {
        return dateA > dateB
    },

    // Check dateA là ngày hôm trước của dateB
    isBeforeDate(dateA, dateB) {
        dateA < dateB
    },

    //
    isValidJSON(str) {
        try {
            JSON.parse(str);
            return true;
            } catch (e) {
            return false;
        }
    },

    randomIntegerInRange(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min
    },

    randomNumberInRange(min, max) {
        return Math.random() * (max - min) + min
    },

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms))
    },

    timeTaken(callback) {
        console.time('timeTaken');
        const r = callback();
        console.timeEnd('timeTaken');
        return r;
    },
}

// Copy value in input tag to clipboard
function copy() {
    var $this = event.target
    $this.select()
    document.execCommand('copy')
}
// Cut value in input tag to clipboard
function cut() {
    var $this = event.target
    $this.select()
    document.execCommand('cut')
}
// Paste value to input value
async function paste() {
    var $this = event.target
    var text = await navigator.clipboard.readText()
    $this.value = text
}

function iosCopyToClipboard(el) {
    console.log(el);
    el.select();
    var oldContentEditable = el.contentEditable,
        oldReadOnly = el.readOnly,
        range = document.createRange();

    el.contentEditable = true;
    el.readOnly = false;
    range.selectNodeContents(el);

    var s = window.getSelection();
    s.removeAllRanges();
    s.addRange(range);

    el.setSelectionRange(0, 999999); // A big number, to cover anything that could be inside the element.

    el.contentEditable = oldContentEditable;
    el.readOnly = oldReadOnly;

    document.execCommand('copy');
}

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

module.exports = funcs