module.exports = {
    // Lấy ngày trong năm
    dayOfYear(date) {
        return Math.floor((date - new Date(date.getFullYear(), 0, 0)) / 1000 / 60 / 60 / 24)
    },

    // Lấy thời gian dạng (hh:mm:ss)
    getTimeFromDate(date) {
        return date.toTimeString().slice(0, 8)
    },

    // Lấy số ngày giữa 2 ngày
    getDaysDiffBetweenDates(dateInitial, dateFinal) {
        return (dateFinal - dateInitial) / (1000 * 3600 * 24)
    },
    
    tomorrow() {
        let t = new Date();
        t.setDate(t.getDate() + 1);
        return t.toISOString().split('T')[0];
    },
}