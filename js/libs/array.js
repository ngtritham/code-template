const funcs = {
    // allEqual: Check mọi phần từ đều = nhau
    allEqual(arr){
        return arr.every(val => val === arr[0])
    },

    // approximatelyEqual: Check 2 số xấp xỉ = (epsilon: 0.001)
    approximatelyEqual(v1, v2, epsilon = 0.001) {
        return Math.abs(v1 - v2) < epsilon
    },

    // average: Tính giá trung bình các số trong mảng
    average(...nums){
        return nums.reduce((acc, val) => acc + val, 0) / nums.length
    },

    // Loại bỏ mọi phần tử giá trị false
    removeAllFalsy(arr){
        return arr.filter(Boolean)
    },

    // countOccurrences: Đếm số lần xuất hiện của giá trị <val>
    countOccurrences(arr, val) {
        return arr.reduce((a, v) => (v === val ? a + 1 : a), 0)
    },

    flatten(arr, depth = 1) {
        return arr.reduce((a, v) => a.concat(depth > 1 && Array.isArray(v) ? flatten(v, depth - 1) : v), [])
    },

    // deepFlatten: Tạo ra mảng 1 chiều chứa toàn bộ phần tử của mảng đầu vào (đa chiều)
    deepFlatten(arr){
        return [].concat(...arr.map(v => (Array.isArray(v) ? deepFlatten(v) : v)))
    },

    // difference: Trả ra mảng các phần tử KHÔNG cùng thuộc 2 mảng
    difference(arr_1, arr_2) {
        const s = new Set(arr_2)
        return arr_1.filter(x => !s.has(x))
    },

    // intersection: Trả ra mảng các phần tử cùng thuộc 2 mảng
    intersection(a, b) {
        const s = new Set(b)
        return a.filter(x => s.has(x))
    },

    // removeDuplicate: Loại bỏ những phần tử bị duplicate
    // Exp: [1, 2, 2, 3, 4, 4, 5] -> [1, 2, 3, 4, 5]
    removeDuplicate(arr) {
        return [ ...new Set(arr)]
    },

    // Trả về tất cả index mà val xuát hiện
    indexOfAll(arr, val) {
        return arr.reduce((acc, el, i) => (el === val ? [...acc, i] : acc), [])
    },

    // Lấy n phần tử lớn nhất
    maxN(arr, n = 1) {
        return [...arr].sort((a, b) => b - a).slice(0, n)
    },

    minN(arr, n = 1) {
        return [...arr].sort((a, b) => a - b).slice(0, n)
    },

    randomIntArrayInRange(min, max, n = 1) {
        return Array.from({ length: n }, () => Math.floor(Math.random() * (max - min + 1)) + min)
    },

    union(a, b) {
        return Array.from(new Set([...a, ...b]));
    },
}

module.exports = funcs