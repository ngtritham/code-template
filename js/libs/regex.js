const MATCH_WHITE_SPACE_AND_NEW_LINE = /[\n\r\s]+/g
const MATCH_A_WORD = /\b(\w*work\w*)\b/g
const MATCH_BETWEEN_HTML_TAG = /<\/?[\w\s]*>|<.+[\W]>/g
const MATCH_BETWEEN_2_DOUBLE_QUOTED = /"\/?[\w\s]*>|<.+[\W]"/g
const MATCH_EVERY_UNICODE_ASCII_WHITESPACE = /[a-zA-Z0-9\u00C0-\u1EF9 ]/g
const MATCH_NOT_UNICODE_ASCII_WHITESPACE = /[^a-zA-Z0-9\u00C0-\u1EF9 ]/g