const funcs = {
    // digitize: Tách số n thành mảng các chữ số của n
    // Exp: 123 -> [1, 2, 3]
    digitize(n = null) {
        return n == null ? [] : [...`${n}`].map(i => parseInt(i))
    },

    // toCurrency(123456.789, 'EUR'); // €123,456.79  | currency: Euro | currencyLangFormat: Local
    // toCurrency(123456.789, 'USD', 'en-us'); // $123,456.79  | currency: US Dollar | currencyLangFormat: English (United States)
    // toCurrency(123456.789, 'USD', 'fa'); // ۱۲۳٬۴۵۶٫۷۹ ؜$ | currency: US Dollar | currencyLangFormat: Farsi
    // toCurrency(322342436423.2435, 'JPY'); // ¥322,342,436,423 | currency: Japanese Yen | currencyLangFormat: Local
    // toCurrency(322342436423.2435, 'JPY', 'fi'); // 322 342 436 423 ¥ | currency: Japanese Yen | currencyLangFormat: Finnish
    toCurrency(n, curr, LanguageFormat = undefined) {
        return Intl.NumberFormat(LanguageFormat, { style: 'currency', currency: curr }).format(n)
    },

    // toDecimalMark(12305030388.9087); // "12,305,030,388.909"
    toDecimalMark(num) {
        return num.toLocaleString('en-US')
    },

    
}

module.exports = funcs