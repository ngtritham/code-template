const funcs = {
    // 1. byteSize: Trả về độ dài của String theo byte
    byteSize(str) {
        return new Blob([str]).size
    },

    // 2. capitalize: Viết hoa chữ cái đầu tiên của câu
    capitalize([first, ...rest]) {
        return first.toUpperCase() + rest.join('')
    },

    // 3. capitalizeEveryWord: Viết hoa chữ cái đầu tiên của mỗi từ
    capitalizeEveryWord(str){
        return str.replace(/\b[a-z]/g, char => char.toUpperCase())
    },

    // 4. decapitalize: Chữ cái viết thường
    decapitalize([first, ...rest]) {
        return  first.toLowerCase() + rest.join('')
    },

    // 3. decapitalizeEveryWord: Viết thuongwf chữ cái đầu tiên của mỗi từ
    decapitalizeEveryWord(str){
        return str.replace(/\b[A-Z]/g, char => char.toLowerCase())
    },

    // 5 . splitLines: Tách một chuỗi nhiều dòng thành một mảng hàng.
    splitLines(str){
        return str.split(/\r?\n/)
    },

    // 6 . stripHTMLTags: Xóa thẻ HTML / XML ra khỏi string cho trước. Sử dụng biểu thức chính quy để xóa HTML / XMLthẻ khỏi chuỗi .
    stripHTMLTags(str){
        return str.replace(/<[^>]*>/g, '')
    },

    // 7. sortCharactersInString: Sắp xếp theo thứ tự a -> z các ký tự trong một chuỗi.
    sortCharactersInString(str){
        return [...str].sort((a, b) => a.localeCompare(b)).join('')
    },

    // 8. words: Convert String thành một Array
    words(str, pattern = /[^a-zA-Z-]+/){
        return str.split(pattern).filter(Boolean)
    },

    reverseString(str) {
        return [...str].reverse().join('')
    } 
}

module.exports = funcs